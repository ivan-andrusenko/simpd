'use strict';

var stream = require('stream');
var util = require('util');
var cp = require('child_process');
var os = require('os');

var vinyl = require('vinyl');

//

function DUglifyPool() {
    this._workers = os.cpus().map(function () {
        return cp.fork(__dirname + '/duglify-worker.js').on('message', this._workerHandler.bind(this));
    }, this);
    this._workerIndex = 0;
    this._jobIndex = 0;
    this._jobDones = {};
}

DUglifyPool.prototype.close = function () {
    this._workers.forEach(function (worker) {
        worker.kill();
    });
    this._workers = null;
};

DUglifyPool.prototype.handle = function (chunk, done) {
    var wi = this._workerIndex++;
    if (this._workerIndex >= this._workers.length) {
        this._workerIndex = 0;
    }
    var jobId = this._jobIndex++;
    this._postDone(jobId, done);
    this._workers[wi].send({
        jobId: jobId,
        file: chunk
    });
};

DUglifyPool.prototype._postDone = function (jobId, done) {
    this._jobDones[jobId] = done;
};

DUglifyPool.prototype._doneJob = function (jobId, chunk) {
    var done = this._jobDones[jobId];
    delete this._jobDones[jobId];
    done(null, chunk);
};

DUglifyPool.prototype._workerHandler = function (msg) {
    var file = new vinyl(msg.file);
    this._doneJob(msg.jobId, file);
};

// DUglify

function DUglify() {
    stream.Transform.call(this, {objectMode: true});
    this._pool = new DUglifyPool();
    this.on('error', function (err) {
        console.log(err);
    });
}
util.inherits(DUglify, stream.Transform);

DUglify.prototype._transform = function (chunk, encoding, done) {
    /*console.log(typeof(chunk));
     console.log(chunk);
     console.log(Object.keys(chunk).join(', '));
     //console.log(JSON.stringify(chunk, null, 2));
     process.exit();*/
    this._pool.handle(chunk, function (err, chunk) {
        this.push(chunk);
    }.bind(this));
    done();
};

DUglify.prototype._flush = function (done) {
    this._pool.close();
    this._pool = null;
    done();
};

//

module.exports = function () {
    return new DUglify();
};