'use strict';

var fs = require('fs');
var path = require('path');
var async = require('async');
var Q = require('q');
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
//var duglify = require('./duglify');
var sass = require('gulp-sass');
var csso = require('gulp-csso');
var templateCache = require('gulp-angular-templatecache');

function loadIndex(fileName) {
    var deferred = Q.defer();
    fs.readFile(fileName, function (err, data) {
        if (err) return deferred.reject(err);
        else deferred.resolve(data.toString());
    });
    return deferred.promise;
}

function saveIndex(fileName, data, done) {
    var deferred = Q.defer();
    fs.writeFile(fileName, data, function (err) {
        if (err) deferred.reject(err);
        else deferred.resolve();
    });
    return deferred.promise;
}

function getScripts(data) {
    var result = [];
    var re = /<script.*?src="(.*?)".*?<\/script>\r?\n?/gmi;
    var script;
    while (script = re.exec(data)) {
        result.push(script[1]);
    }
    return result;
}

function getStyles(data) {
    var result = [];
    var re = /<link\s*?rel="stylesheet"\s*?href="(.*?)"\/*?>/gmi;
    var style;
    while (style = re.exec(data)) {
        result.push(style[1]);
    }
    return result;
}

function getCopies(data) {
    var result = [];
    var re = /<!-- copy: (.*?) to: (.*?) -->/gmi;
    var style;
    while (style = re.exec(data)) {
        result.push({from: style[1], to: style[2]});
    }
    return result;
}

function setScripts(data, name) {
    var re = /<script[\s\w\W\S]*<\/script>/gmi;
    return data.replace(re, '<script src="' + name + '"></script>');
}

function setStyles(data, name) {
    var re = /<link[\s\w\W\S]*<\/link>/gmi;
    return data.replace(re, '<link rel="stylesheet" href="' + name + '"/>');
}

function buildTemplates(options, data) {
    var deferred = Q.defer();
    var source = [
        path.join(options.source, '**/views/**/*.html'),
        path.join(options.source, '**/blocks/**/*.html'),
        path.join(options.source, '**/partials/**/*.html')
    ];
    gulp.src(source)
        .pipe(templateCache({standalone: true}))
        .pipe(gulp.dest(options.source)).on('end', function () {
        deferred.resolve(data);
    });
    return deferred.promise;
}

function buildScripts(options, name, scripts) {
    var deferred = Q.defer();
    var source = gulp.src(scripts);
    if (options.production) source = source.pipe(uglify().on('error', function (err) {
        console.log(err);
    }));
    source = source.pipe(concat(name));
    source.pipe(gulp.dest(options.destination))
        .on('end', function () {
            deferred.resolve();
        })
        .on('error', function (err) {
            deferred.reject(err);
        });
    return deferred.promise;
}

function buildStyles(options, name, styles) {
    var deferred = Q.defer();
    console.log(styles);
    gulp.src(styles)
        .pipe(sass().on('error', function () {
            console.log(arguments);
        }))
        .pipe(concat(name).on('error', function () {
            console.log(arguments);
        }))
        .pipe(csso().on('error', function () {
            console.log(arguments);
        }))
        .pipe(gulp.dest(options.destination))
        .on('end', function () {
            deferred.resolve();
        })
        .on('error', function (err) {
            deferred.reject(err);
        });
    return deferred.promise;
}

function buildCopies(options, name, values) {
    var deferred = Q.defer();
    async.each(values, function (item, done) {
        gulp.src(item.from)
            .pipe(gulp.dest(item.to))
            .on('end', function () {
                done();
            })
            .on('error', function (err) {
                done(err);
            });
    }, function (err) {
        if (err) deferred.reject(err);
        else deferred.resolve();
    });
    return deferred.promise;
}

function build(options) {
    return function (done) {
        loadIndex(path.join(options.source, options.indexHtml))
            .then(buildTemplates.bind(buildTemplates, options))
            .then(buildProject.bind(buildProject, done))
            .then(function () {
                done();
            }, done).done();
    };
    //

    function buildProject(done, data) {
        var version = new Date().getTime();
        var scripts = getScripts(data).map(function (item) {
            return path.join(options.source, item);
        });
        var styles = getStyles(data).map(function (item) {
            return path.join(options.source, item);
        });
        var copies = getCopies(data).map(function (item) {
            return {
                from: path.join(options.source, item.from),
                to: path.join(options.destination, item.to)
            };
        });
        var scriptVersion = options.versions && options.versions.script ? options.versions.script : version;
        var styleVersion = options.versions && options.versions.style ? options.versions.style : version;
        var scriptName = 'app.' + scriptVersion + '.js';
        var styleName = 'style.' + styleVersion + '.css';
        return Q.all([
            buildScripts(options, scriptName, scripts),
            buildStyles(options, styleName, styles),
            buildCopies(options, '', copies)
        ]).then(function () {
            data = setScripts(data, scriptName);
            data = setStyles(data, styleName);
            return saveIndex(path.join(options.destination, options.indexHtml), data);
        }, done);
    }
}

function watch(options) {
    var deferred = Q.defer();
    loadIndex(path.join(options.source, options.indexHtml)).then(function (data) {
        var result = [
            options.indexHtml
        ];
        var version = new Date().getTime();
        var scripts = getScripts(data).map(function (item) {
            return path.join(options.source, item);
        });
        var styles = getStyles(data).map(function (item) {
            return path.join(options.source, item);
        });
        scripts.forEach(function (item) {
            result.push(item);
        });
        styles.forEach(function (item) {
            result.push(item);
        });
        return result;
    }).then(function (items) {
        items.map(function (item) {
            return path.join(options.source, item);
        }).forEach(function (item) {
            fs.watchFile(item, {persistent: true, interval: 10}, watchListener);
        });
        function removeWatchers() {
            items.forEach(function (item) {
                fs.unwatchFile(item);
            });
        }

        function watchListener() {
            removeWatchers();
            deferred.resolve();
        }
    });
    return deferred.promise;
}

module.exports = build;
module.exports.watch = watch;
