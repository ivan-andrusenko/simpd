'use strict';

var uglify = require('gulp-uglify');
var vinyl = require('vinyl');

uglify = uglify();

var queue = [];

uglify.on('error', function (err) {
    //console.log('err');
    //console.log(err);
});

uglify.on('data', function (chunk) {
    var job = queue.shift();
//  console.log('WORKER processed '+job.jobId);
    process.send({
        jobId: job.jobId,
        file: chunk
    });
});

process.on('message', function (msg) {
    //console.log('WORKER processing '+msg.jobId);
    var file = new vinyl(msg.file);
    //console.log(file);
    queue.push(msg);
    uglify.write(file);
})