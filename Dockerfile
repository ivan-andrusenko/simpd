FROM node:8-alpine


WORKDIR /usr/src/app

COPY package*.json ./

RUN apk add --no-cache --virtual .gyp \
        python \
        make \
        g++ \
    && npm install -g gulp \
    && npm install \
    && npm rebuild bcrypt --build-from-source \
    && npm rebuild \
    && apk del .gyp

COPY . .
#RUN npm rebuild
RUN gulp
EXPOSE 8124
CMD [ "npm", "start" ]
