(function (window, document, undefined) {
  'use strict';

  angular.module('simpd', ['ng', 'ngCookies', 'pascalprecht.translate',
    'tc.chartjs', 'templates', 'ngSanitize',
    'webapp', 'ui.router', 'ui.bootstrap',
    'ui.tree', 'ui.select'])

    .config(['webappSessionProvider', '$stateProvider', '$urlRouterProvider', '$translateProvider', '$httpProvider',
      function (webappSessionProvider, $stateProvider, $urlRouterProvider, $translateProvider, $httpProvider) {
        var prevToken = localStorage.getItem("token");
        if (prevToken) {
          // console.log(prevToken);
          webappSessionProvider.token(prevToken);
        }

        $.material.init();
        imageMapResize();


        MathJax.HTML.Cookie.Set("menu", {});
        MathJax.Hub.Config({
          skipStartupTypeset: true,
          messageStyle: "none",
          extensions: ["tex2jax.js", "mml2jax.js", "MathML/content-mathml.js", "MathML/mml3.js"],
          jax: ["input/MathML", "input/TeX", "output/SVG", "output/HTML-CSS", "output/NativeMML", "output/CommonHTML"],
          "HTML-CSS": {
            availableFonts: [],
            styles: {".MathJax_Preview": {visibility: "hidden"}},
            showMathMenu: false
          },
          "SVG": {
            availableFonts: [],
            styles: {".MathJax_Preview": {visibility: "hidden"}},
            showMathMenu: false
          },
          "NativeMML": {
            availableFonts: [],
            styles: {".MathJax_Preview": {visibility: "hidden"}},
            showMathMenu: false
          },
          "CommonHTML": {
            availableFonts: [],
            styles: {".MathJax_Preview": {visibility: "hidden"}},
            showMathMenu: false
          }
        });
        MathJax.Hub.Register.StartupHook("HTML-CSS Jax Ready", function () {
          var FONT = MathJax.OutputJax["HTML-CSS"].Font;
          FONT.loadError = function (font) {
            MathJax.Message.Set("Can't load web font TeX/" + font.directory, null, 2000);
            document.getElementById("noWebFont").style.display = "";
          };
          FONT.firefoxFontError = function (font) {
            MathJax.Message.Set("Firefox can't load web fonts from a remote host", null, 3000);
            document.getElementById("ffWebFont").style.display = "";
          };
        });

        (function (HUB) {

          var MINVERSION = {
            Firefox: 3.0,
            Opera: 9.52,
            MSIE: 6.0,
            Chrome: 0.3,
            Safari: 2.0,
            Konqueror: 4.0,
            Unknown: 10000.0 // always disable unknown browsers
          };

          if (!HUB.Browser.versionAtLeast(MINVERSION[HUB.Browser] || 0.0)) {
            HUB.Config({
              jax: [],                   // don't load any Jax
              extensions: [],            // don't load any extensions
              "v1.0-compatible": false   // skip warning message due to no jax
            });
            setTimeout('document.getElementById("badBrowser").style.display = ""', 0);
          }

        })(MathJax.Hub);

        MathJax.Hub.Register.StartupHook("End", function () {
          var HTMLCSS = MathJax.OutputJax["HTML-CSS"];
          if (HTMLCSS && HTMLCSS.imgFonts) {
            document.getElementById("imageFonts").style.display = ""
          }
        });

        $translateProvider.useStaticFilesLoader({
          prefix: 'locales/',
          suffix: '.json'
        });
        $translateProvider.useLocalStorage();
        $translateProvider.preferredLanguage('en');
        $stateProvider.state({
          name: 'signup',
          url: '/signup',
          template: '<div data-simpd-signup></div>'
        }).state({
          name: 'signin',
          url: '/signin',
          template: '<div data-simpd-signin></div>'
        }).state({
          name: 'main',
          template: '<div data-simpd-main></div>'
        }).state({
          name: 'enter',
          url: '/',
          template: '<div data-simpd-enter></div>'
        }).state({
          name: 'dashboard',
          url: '/dashboard',
          parent: 'main',
          template: '<div data-simpd-dashboard></div>'
        }).state({
          name: 'authors',
          url: '/authors',
          parent: 'main',
          template: '<div data-simpd-authors></div>'
        }).state({
          name: 'current-profiles',
          url: '/current-profiles',
          parent: 'main',
          template: '<div data-simpd-current-profiles></div>'
        }).state({
          name: 'standard-base',
          url: '/standard-base',
          parent: 'main',
          template: '<div data-simpd-standard-base></div>'
        }).state({
          name: 'hierarchies',
          url: '/hierarchies',
          parent: 'main',
          template: '<div data-simpd-hierarchy-list data-source="items" data-add-item="addItem()"></div>',
          resolve: {
            items: ['$http', '$stateParams', function ($http, $stateParams) {
              return $http.get('/api/v0/requirements').then(function (result) {
                return result.data;
              });
            }]
          },
          controller: ['$scope', '$state', 'items', function ($scope, $state, items) {
            $scope.items = items;
            $scope.addItem = function () {
              $state.go('hierarchyEditor', {id: 'new'});
            };
          }]
        }).state({
          name: 'hierarchyEditor',
          url: '/hierarchies/:id',
          parent: 'main',
          params: {
            id: null
          },
          template: '<div data-simpd-hierarchy-editor data-value="value"></div>',
          resolve: {
            value: ['$http', '$stateParams', function ($http, $stateParams) {
              if ($stateParams.id != 'new') return $http.get('/api/v0/requirements/' + $stateParams.id).then(function (result) {
                return result.data;
              });
              else return {
                id: null,
                name: '',
                items: []
              };
            }]
          },
          controller: ['$scope', 'value', function ($scope, value) {
            $scope.value = value;
          }]
          // }).state({
          //     name: 'evaluation',
          //     url: '/evaluation',
          //     parent: 'main',
          //     template: '<div data-simpd-evaluation data-value="evaluation"></div>',
          //     resolve: {
          //       evaluation: ['$modal', '$http', function ($modal, $http) {
          //         var instance = $modal.open({
          //           size: 'lg',
          //           templateUrl: 'app/blocks/new-evaluation-popup/template.html',
          //           resolve: {
          //             requirements: ['$http', function ($http) {
          //               return $http.get('/api/v0/requirements').then(function (result) {
          //                 return result.data;
          //               });
          //             }]
          //           },
          //           controller: ['$scope', 'requirements', function ($scope, requirements) {
          //             $scope.requirements = requirements;
          //             $scope.submit = function () {
          //               console.log('submit');
          //               var result = $scope.evaluation;
          //               $scope.$close(result);
          //             };
          //           }]
          //         });
          //         return instance.result.then(function (result) {
          //           console.log('creating evaluation');
          //           console.log(result);
          //           return $http.get('/api/v0/requirements/' + result.requirements_id).then(function (res) {
          //             result.requirements = res.data;
          //             return result;
          //           })
          //         });
          //       }]
          //     },
          //     controller: ['$scope', 'evaluation', function ($scope, evaluation) {
          //       console.log('evaluation: ');
          //       console.log(evaluation);
          //       $scope.evaluation = evaluation;
          //     }]
          //   }).state({
          //   name: 'evaluationSaved',
          //   url: '/evaluation-done',
          //   parent: 'main',
          //   template: '<h4> hay </h4>'
        }).state({
          name: 'evaluations',
          url: '/evaluations',
          parent: 'main',
          template: '<div data-simpd-evaluation-list data-source="items" data-add-item="addItem()"></div>',
          resolve: {
            items: ['$http', '$stateParams', function ($http, $stateParams) {
              return $http.get('/api/v0/evaluations').then(function (result) {
                return result.data;
              });
            }]
          },
          controller: ['$scope', '$state', 'items', function ($scope, $state, items) {
            $scope.items = items;
            $scope.addItem = function () {
              $state.go('evaluation');
            };
          }]
        }).state({
          name: 'evaluationsEditor',
          url: '/evaluations/:id?view',
          parent: 'main',
          params: {
            id: null,
            view: null
          },
          template: '<div data-simpd-evaluation-editor data-value="value" data-view="view"></div>',
          resolve: {
            value: ['$http', '$stateParams', function ($http, $stateParams) {
              if ($stateParams.id != 'new') return $http.get('/api/v0/evaluations/' + $stateParams.id).then(function (result) {
                return result.data;
              });
              else return {};
            }]
          },
          controller: ['$scope', 'value', function ($scope, value) {
            $scope.value = value;
          }]
        }).state({
          name: 'researches',
          url: '/researches',
          parent: 'main',
          template: '<div data-simpd-research-list data-source="items"></div>',
          resolve: {
            items: ['$http', function ($http) {
              return $http.get('/api/v0/researches').then(function (result) {
                return result.data;
              });
            }]
          },
          controller: ['$scope', 'items', function ($scope, items) {
            $scope.items = items;
          }]
        }).state({
          name: 'researches.editor',
          url: '/researches/:id',
          params: {
            id: null,
            source: null
          },
          parent: 'main',
          template: '<div data-simpd-research-editor data-value="value"></div>',
          resolve: {
            value: ['$http', '$stateParams', function ($http, $stateParams) {
              if ($stateParams.id == 'new') return $stateParams.source;
              else return $http.get('/api/v0/researches/' + $stateParams.id).then(function (result) {
                return result.data;
              });
            }]
          },
          controller: ['$scope', 'value', function ($scope, value) {
            $scope.value = value;
          }]
        }).state({
          name: 'researches_evaluation_editor',
          url: '/researches/:researchId/evaluations/:evaluationId',
          parent: 'main',
          params: {
            researchId: null,
            evaluationId: null
          },
          template: '<div data-simpd-evaluation data-research-value="evaluation"></div>',
          resolve: {
            // evaluation: ['webappSession', function (webappSession) {
            //   evaluation.expert_name = webappSession.getUser();
            //   return evaluation
            // }]
            evaluation: ['$modal', '$http', '$stateParams', 'webappSession',
              function ($modal, $http, $stateParams, webappSession) {
                console.log("Before return webappSession.getUser().then(function (re)");
                return webappSession.getUser()
                  .then(function (re) {
                    // console.log("re.name");
                    // console.log(re.name);
                    // if (expert.role != "user") {
                    //   var options = {
                    //     content: "Only experts can evaluate",
                    //     style: "snackbar",
                    //     timeout: 6000
                    //   };
                    //   $.snackbar(options);
                    // }
                    console.log(re);
                    var expertName = re.name;
                    console.log(re);
                    var instance = {
                      result: Promise.resolve({
                        expertName: expertName,
                        expertEducation: ""
                      })
                    };
                    // console.log(instance);
                    return instance.result.then(function (result) {
                      // console.log('loading research');
                      return loadResearch()
                        .then(function (research) {
                          // console.log('loading requirements');
                          return $http.get('/api/v0/requirements/' + research.requirements_id)
                            .then(function (res) {
                              var newEvaluation = {
                                expertName: result.expertName,
                                expertEducation: result.expertEducation,
                                requirements: res.data
                              };
                              if (!research.items) research.items = [];
                              research.items.push(newEvaluation);
                              // console.log('resolved');
                              // console.log(resolved);
                              return {
                                research: research,
                                evaluation: newEvaluation
                              };
                            });
                        });
                      function loadResearch() {
                        return $http.get('/api/v0/researches/' + $stateParams.researchId).then(function (result) {
                          // console.log('research data');
                          // console.log(result.data);
                          return result.data;
                        });
                      }
                    });
                  }, function (err) {
                    console.log(err);
                  });
              }]
          },
          controller: ['$scope', 'evaluation', function ($scope, evaluation) {
            // console.log('evaluation: ');
            // console.log(evaluation);
            $scope.evaluation = evaluation;
          }]
        }).state({
          name: 'research_results',
          url: '/research/:id/results',
          parent: 'main',
          params: {
            id: null
          },
          template: '<div data-simpd-research-result data-research="research" data-metrics="metrics"></div>',
          resolve: {
            research: ['$http', '$stateParams', function ($http, $stateParams) {
              return $http.get('/api/v0/researches/' + $stateParams.id).then(function (result) {
                return result.data;
              });
            }],
            metrics: ['$http', function ($http) {
              return $http.get('/api/v0/metrics').then(function (result) {
                // console.log("in resolver: ");
                // console.log(result.data);
                return result.data;
              });
            }]
          },
          controller: ['$scope', 'research', 'metrics', function ($scope, research, metrics) {
            $scope.research = research;
            $scope.metrics = metrics;
          }]
        }).state({
          name: 'metrics',
          url: '/metrics',
          parent: 'main',
          template: '<div data-simpd-metrics-list data-source="items"></div>',
          resolve: {
            items: ['$http', function ($http) {
              return $http.get('/api/v0/metrics').then(function (result) {
                return result.data;
              });
            }]
          },
          controller: ['$scope', 'items', function ($scope, items) {
            $scope.items = items;
          }]
        }).state({
          name: 'metrics.editor',
          url: '/metrics/:id',
          params: {
            id: null,
            source: null
          },
          parent: 'main',
          template: '<div data-simpd-metrics-editor data-value="value"></div>',
          resolve: {
            value: ['$http', '$stateParams', function ($http, $stateParams) {
              if ($stateParams.id == 'new') return $stateParams.source;
              else return $http.get('/api/v0/metrics/' + $stateParams.id).then(function (result) {
                return result.data;
              });
            }]
          },
          controller: ['$scope', 'value', function ($scope, value) {
            // console.log("in state({name: metrics.editor})");
            $scope.value = value;
          }]
        });
        $urlRouterProvider.otherwise('/');
      }]);
})(window, document);