(function (window, document, undefined) {

    'use strict';

    angular.module('simpd').directive('simpdEvaluation', function () {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                value: '=',
                researchValue: '='
            },
            templateUrl: 'app/blocks/evaluation/template.html',
            controller: ['$scope', '$http', '$state', '$rootScope', function ($scope, $http, $state, $rootScope) {
                $scope.stack = [];
                $scope.levelsLoading = null;
                var levels = {};
                console.log($scope);
                $scope.go = $rootScope.go;

                if ($scope.researchValue) {
                    $scope.source = $scope.researchValue.evaluation.requirements;
                    $scope.value = $scope.researchValue.evaluation;
                }

                if (!$scope.levelsLoading) {
                    $scope.levelsLoading = getLevelsLoading($scope.source);
                }

                $scope.value.date = new Date();
                $scope.source.rate = 1;
                $scope.done = false;
                $scope.$watch('source', function (value) {
                    function calcItem(item) {
                        var all = true;
                        var max = 0;
                        var sum = 0;
                        if (item.items && item.items.length) {
                            for (var k = 0; k < item.items.length; k++) {
                                var i = item.items[k];
                                if (!i.rate) {
                                    all = false;
                                    continue;
                                }
                                if (i.rate > max) max = i.rate;
                                sum += i.rate;
                            }
                            if (all) {
                                for (var k = 0; k < item.items.length; k++) {
                                    var i = item.items[k];
                                    console.log('rating{' + i.level + '} = ' + i.rate + ' / ' + $scope.levelsLoading[i.level - 1] + ' = ' + i.rate / $scope.levelsLoading[i.level]);
                                    i.value = i.rate / sum / ( i.level > 0 ? $scope.levelsLoading[i.level - 1] : 1);
                                    //i.value = i.rate / sum;
                                }
                            }
                            for (var k = 0; k < item.items.length; k++) {
                                var i = item.items[k];
                                calcItem(i);
                            }

                        }

                        function trace(root, root_level, data) {
                            if (root.items) {
                                for (var i in root.items) {
                                    trace(root.items[i], root_level + 1, data);
                                }

                                data[root_level] += root.value;
                            }
                        }

                        for (var i = 1; i < 6; ++i) {
                            levels[i] = 0.0;
                        }
                        trace($scope.source, 1, levels);
                        console.log('levels:');
                        console.log(levels);
                    }

                    calcItem(value);
                    if (isReady(value)) {
                        $scope.done = true;
                    }
                }, true);
                $scope.setRateItem = function (value, parent) {
                    if (parent) $scope.stack.push(parent);
                    $scope.rateItem = value;
                    $scope.maxValue = 10;//value.items.length;
                    $scope.restValue = $scope.maxValue;
                };
                $scope.goBack = function () {
                    var item = $scope.stack[$scope.stack.length - 1];
                    $scope.stack.splice($scope.stack.length - 1, 1);
                    $scope.setRateItem(item);
                };
                function isReady(item) {
                    if (!item.rate) return false;
                    if (item.items) return item.items.filter(isReady).length == item.items.length;
                    else return true;
                }

                function getLevelsLoading(root) {
                    function traceTree(root, root_level, data) {
                        if (root.items.length) {
                            for (var i in root.items) {
                                traceTree(root.items[i], root_level + 1, data);
                            }
                            ++data[root_level];
                        }
                        root.level = root_level;
                    }
                    var res = {};
                    for (var i = 1; i < 6; ++i) {
                        res[i] = 0;
                    }
                    traceTree(root, 1, res);
                    return res;
                }

                $scope.getIndicatorClass = function (item) {
                    if (isReady(item)) return 'glyphicon-ok';
                    else return 'glyphicon-time';
                };
                $scope.setRateItem($scope.source);
                $scope.save = function () {
                    if ($scope.researchValue) {
                        $http.put('/api/v0/researches/' + $scope.researchValue.research.id, $scope.researchValue.research).then(function () {
                            $state.go('researches.editor', {id: $scope.researchValue.research.id});
                        });
                    }
                    else {
                        $http.post('/api/v0/evaluations', $scope.value).then(function () {
                            $state.go('evaluationSaved');
                        });
                    }
                };
            }]
        };
    });

})(window, document);