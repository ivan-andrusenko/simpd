(function (window, document, undefined) {

    'use strict';

    angular.module('simpd').directive('simpdMetricsList', function () {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: 'app/blocks/metrics-list/template.html',
            scope: {
                source: '='
            },
            controller: ['$scope', '$modal', '$http', '$state', '$rootScope', function ($scope, $modal, $http, $state, $rootScope) {

                $scope.go = $rootScope.go;
                $scope.delete = function (item) {
                    if (confirm('Удалить метрики?')) {
                        $http.delete('/api/v0/metrics/' + item.id).then(function () {
                            var i = $scope.source.indexOf(item);
                            if (i >= 0) $scope.source.splice(i, 1);
                        });
                    }
                };
                $scope.addItem = function () {
                    var instance = $modal.open({
                        size: 'lg',
                        templateUrl: 'app/blocks/new-metrics-popup/template.html',
                        resolve: {
                            requirements: ['$http', function ($http) {
                                return $http.get('/api/v0/requirements').then(function (result) {
                                    return result.data;
                                });
                            }]
                        },
                        controller: ['$scope', 'requirements', function ($scope, requirements) {
                            $scope.metrics = {};
                            $scope.requirements = requirements;
                            $scope.submit = function () {
                                var result = $scope.metrics;
                                $scope.$close(result);
                            };
                        }]
                    });
                    instance.result.then(function (result) {
                        return $http.get('/api/v0/requirements/' + result.requirements_id).then(function (res) {
                            result.requirements = res.data;
                            result.date = new Date();
                            return result;
                        }).then(function (metrics) {
                            $state.go('metrics.editor', {id: 'new', source: metrics});
                        });
                    });
                };
            }]
        };
    });

})(window, document);