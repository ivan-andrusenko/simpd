(function (window, document, undefined) {

  'use strict';

  angular.module('simpd').directive('simpdSignin', function () {
    return {
      restrict: 'EA',
      replace: true,
      scope: true,
      templateUrl: 'app/blocks/signin/template.html',
      controller: ['$scope', '$state', '$http', 'webappSession', function ($scope, $state, $http, webappSession) {
        $scope.credentials = {};
        $scope.back = function () {
          $state.go('enter');
        };
        $scope.signin = function () {
          webappSession.signin($scope.credentials.name, $scope.credentials.password).then(function (res) {
            console.log(res);
            if (res.error) {
              var options = {
                content: res.error,
                style: "snackbar", // add a custom class to your snackbar
                timeout: 6000
              };
              $.snackbar(options);
            } else {
              $state.go('dashboard');
            }
          });
        };
      }]
    };
  });

})(window, document);