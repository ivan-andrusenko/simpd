(function(window, document, undefined) {

'use strict;'

angular.module('simpd')
  .directive('simpdStandardBase', function() {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: 'app/blocks/standard-base/template.html'
    };
  });

})(window, document);