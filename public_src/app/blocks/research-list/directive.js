(function(window, document, undefined) {

'use strict';

angular.module('simpd').directive('simpdResearchList', function(){
  return {
    restrict: 'EA',
    replace: true,
    templateUrl: 'app/blocks/research-list/template.html',
    scope: {
      source: '='
    },
    controller: ['$scope', '$modal', '$http', '$state', '$rootScope', function($scope, $modal, $http, $state, $rootScope) {
      $scope.delete = function(item) {
       if (confirm('Удалить исследование?')) {
          $http.delete('/api/v0/researches/'+item.id).then(function() {
            var i = $scope.source.indexOf(item);
            if (i >= 0) $scope.source.splice(i, 1);
          });
        }
      };
      $scope.go = $rootScope.go;
      $scope.addItem = function() {
        var instance = $modal.open({
          size: 'lg',
          templateUrl: 'app/blocks/new-research-popup/template.html',
          resolve: {
            requirements: ['$http', function($http) {
              return $http.get('/api/v0/requirements').then(function(result) {
                return result.data;
              });
            }]
          },
          controller: ['$scope', 'requirements', function($scope, requirements) {
            $scope.research = {};
            $scope.requirements = requirements;
            $scope.submit = function() {
              var result = $scope.research;
              $scope.$close(result);
            };
          }]
        });
        instance.result.then(function(result) {
          return $http.get('/api/v0/requirements/'+result.requirements_id).then(function(res) {
            result.requirements = res.data;
            result.date = new Date();
            return result;
          }).then(function(research) {
            console.log('result');
            console.log(result);

            $state.go('researches.editor', {id: 'new', source: research});
          });
        });
      };
    }]
  };
});

})(window, document);