(function(window, document, undefined) {

'use strict';

angular.module('simpd')
  .directive('simpdCurrentProfiles', function() {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: 'app/blocks/current-profiles/template.html',
      controller: ['$scope', '$rootScope', '$location', '$anchorScroll', function($scope, $rootScope, $location, $anchorScroll) {

        // $('.simpd-background').addClass('highlight');
        // $scope.$on('$destroy', function() {
        //   $('.simpd-background').removeClass('highlight');
        // });
        // $rootScope.go = function (path) {
        //   $location.path(path);
        // };

        $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
          if($location.hash()) $anchorScroll();
        });
        $scope.scroll = function(id) {
          // set the location.hash to the id of
          // the element you wish to scroll to.
          $location.hash(id);

          // call $anchorScroll()
          $anchorScroll();
        };
        $('map').imageMapResize();
      }]
    };
  });

})(window, document);