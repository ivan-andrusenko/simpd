(function (window, document, undefined) {

    'use strict';

    angular.module('simpd').directive('simpdResearchResult', function () {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: 'app/blocks/research-result/template.html',
            scope: {
                research: '=',
                metrics: '='
            },
            controller: ['$scope', '$http', '$q', 'webappExcelGenerator', function ($scope, $http, $q, webappExcelGenerator) {
                $scope.metrics = getMetrics($scope.research.requirements_id);
                console.log('research');
                console.log($scope.research);

                // $scope.metrics = $scope.metrics.$$state;
                $scope.view = 'list';
                $scope.value = collapseRequirements($scope.research);
                // console.log("$scope.requirements_id");
                // console.log($scope.requirements_id);
                // console.log("$scope.research");
                // console.log($scope.research);
                // console.log("$scope");
                // console.log($scope);

                // getMetrics($scope.research.requirements_id);
                // $q.all($scope.metrics).then(function (result) {
                //     $scope.metrics = result;
                // });
                // console.log("$scope.metrics");
                // console.log($scope.metrics);
                var flatItems = [];
                processLevel($scope.value[0].items);
                processValue($scope.value[0]);
                $scope.items = flatItems;
                verifyRatingSum(flatItems);
                // $scope.value[0] = (+ $scope.value[0]).toPrecision(5);
                $scope.saveCsv = function () {
                    var headerRow = null;
                    var rows = $scope.items.map(function (item) {
                        var result = [item.number, item.name];
                        if (!headerRow) {
                            headerRow = ['', ''];
                            item.experts.forEach(function (expert) {
                                headerRow.push(expert);
                            });
                        }
                        item.values.forEach(function (value) {
                            result.push(value);
                        });
                        return result;
                    });
                    rows.splice(0, 0, headerRow);
                    var data = webappExcelGenerator.generate(null, rows);
                    var blob = new Blob([data], {type: "application/xml;charset=" + document.characterSet});
                    saveAs(blob, 'result.xml');
                };
                // console.log("$scope.metrics");
                // console.log($scope.metrics);

                function getMetrics(requirementsId) {
                    // return $http.get('/api/v0/metrics').then(function (result) {
                    //     return result.data;
                    // });
                    // return $http.get('/api/v0/metrics/' + researchNumber).then(function (res) {
                    //     return res.data;
                    // });
                    // var result = null;
                    // return $http.get('/api/v0/metrics').then(function (res) {
                        // return res.data;
                    for (var i in $scope.metrics) {
                        // console.log("i: ");
                        // console.log(i);
                        // console.log("$scope.metrics: ");
                        // console.log($scope.metrics);
                        if ($scope.metrics[i].requirements_id == requirementsId) {
                            // console.log("res.data[i]: ");
                            // console.log(res.data[i]);
                            return $scope.metrics[i];
                            // $scope.metrics = res.data[i];
                        }
                    }
                    // });// return result;
                }

                function processLevel(items, level) {
                    if (!level) level = 0;
                    items.forEach(function (item) {
                        flatItems.push({
                            level: level,
                            number: item.number,
                            name: item.name,
                            rate: item.rate,
                            value: average(item.values),
                            values: item.values,
                            experts: item.experts,
                            isLeaf: item.items.length == 0
                        });
                        if (item.items) processLevel(item.items, level + 1);
                    });
                }


                function processValue(root) {
                    root.Q = evaluateIntegralIndex(root);
                    //processStep1(root.items);
                    //processStep2(root.items);
                    //root.Q = calcq(root.items, 'Q');

                    //function processStep1(items) {
                    //    items.forEach(function (item) {
                    //        // children
                    //        if (item.items) processStep1(item.items);
                    //        // current
                    //        item.q = calcq(item.values);
                    //    });
                    //}
                    //
                    //
                    //calcIntegralIndex();

                    //function processStep2(items) {
                    //    items.forEach(function (item) {
                    //        // children
                    //        if (item.items && item.items.length) {
                    //            processStep2(item.items);
                    //            // current
                    //            item.Q = calcQ(item.q, items);
                    //        }
                    //        else {
                    //            item.Q = item.q;
                    //        }
                    //    });
                    //}
                }



                function evaluateLevelIndex(root, level) {
                    var result = 0.0;
                    if (root.level == level - 1) {
                        if (root.items.length) {
                            root.items.forEach(function (item) {
                                var product = 0.0;
                                if (item.items.length) {
                                    item.items.forEach(function (subitem) {
                                        // var metricIntegralIndex = getMetricIndexForNode(subitem.number, $scope.metrics);
                                        // console.log(subitem);
                                        // console.log(metricIntegralIndex);
                                        result += average(subitem.values) * average(item.values) * getMetricIndexForNode(subitem.number, $scope.metrics);
                                        subitem.Q = average(subitem.values); //Added getMetricIndexForNode call here
                                    });
                                }
                                // else
                                //     result += average(item.values);
                                item.Q = average(item.values);// * getMetricIndexForNode(item.number, $scope.metrics);  //Added getMetricIndexForNode call here
                            });
                        }
                    }
                    else if (root.items.length) {
                        root.items.forEach(function (item) {
                            result += evaluateLevelIndex(item, level);
                        });
                    }
                    return result;
                }


                function evaluateIntegralIndex(root) {
                    var levelNum = setNodeLevelsAndGetMaxLevel(root);
                    console.log(levelNum);
                    var levels = {};
                    for (var i = 1; i < levelNum; ++i) {
                        levels[i] = evaluateLevelIndex(root, i);
                    }
                    //console.log(levels);
                    var integralIndex = 0.0;
                    for (var i = 1; i < levelNum; ++i) {
                        integralIndex += levels[i];// * getMetricForNode();
                    }
                    //console.log(integralIndex);
                    return integralIndex;
                }

            }]
        }
    }).controller('radarCtrl', ['$scope', function ($scope) {
        $scope.data = {
            labels: getLeafItemAttributes($scope.items, 'name'),
            datasets: [
                {
                    fillColor: 'rgba(0, 0, 220, 0.6)',
                    strokeColor: 'rgba(0, 0, 220, 1)',
                    pointColor: 'rgba(0, 0, 220, 1)',
                    pointStrokeColor: '#00f',
                    pointHighlightFill: '#00f',
                    pointHighlightStroke: 'rgba(220, 220, 220, 1)',
                    data: getLeafItemAttributes($scope.items, 'value')
                }
            ]
        };
        $scope.options = {
            responsive: true,
            scaleShowLabels: true,
            scaleBeginAtZero: false,
            legendTemplate: ''
        };

    }]);

    function collapseRequirements(research) {
        var result = [];
        var root;
        research.items.forEach(function (ev) {
            var req = ev.requirements;
            if (!root) {
                root = {};
                root.name = req.name;
                result.push(root);
            }
            process(req, root, ev.expertName);
        });
        function process(source, target, expertName) {
            if (!source.items) return;
            var items = target.items;
            if (!items) items = target.items = [];
            var i = -1;
            source.items.forEach(function (src) {
                i++;
                var tgt = items[i];
                if (!tgt) {
                    tgt = {};
                    tgt.number = src.number;
                    tgt.name = src.name;
                    tgt.rates = [];
                    tgt.values = [];
                    tgt.experts = [];
                    items.push(tgt);
                }
                tgt.rates.push(src.rate);
                tgt.values.push(src.value);
                tgt.experts.push(expertName);
                process(src, tgt, expertName);
            });
        }

        return result;
    }

    function getLeafItemAttributes(items, attr) {
        var res = [];
        items.forEach(function (item) {
            if (item.isLeaf)
                res.push(item[attr]);
        });
        return res;
    }

    function getLowLevelItemAttributes(items, attr) {
        var res = [];
        var levels = [];
        items.forEach(function (item) {
            levels.push(item.level);
        });
        var lowLevel = Math.max.apply(Math, levels);
        // console.log(lowLevel);
        items.forEach(function (item) {
            if (item.level == lowLevel)
                res.push(item[attr]);
        });
        return res;
    }

    function processValue(root) {
        root.Q = evaluateIntegralIndex(root);
        //processStep1(root.items);
        //processStep2(root.items);
        //root.Q = calcq(root.items, 'Q');

        //function processStep1(items) {
        //    items.forEach(function (item) {
        //        // children
        //        if (item.items) processStep1(item.items);
        //        // current
        //        item.q = calcq(item.values);
        //    });
        //}
        //
        //
        //calcIntegralIndex();

        //function processStep2(items) {
        //    items.forEach(function (item) {
        //        // children
        //        if (item.items && item.items.length) {
        //            processStep2(item.items);
        //            // current
        //            item.Q = calcQ(item.q, items);
        //        }
        //        else {
        //            item.Q = item.q;
        //        }
        //    });
        //}
    }
    //
    function verifyRatingSum(flatItems) {
        var levels = {};
        for (var i = 1; i < 5; i++) {
            levels[i] = 0.0;
        }
        flatItems.forEach(function (item) {
            levels[item.level + 1] += item.values[0];
            //console.log('number = ' + item.number, 'level = ' + (item.level + 1), 'value = ' + item.value);
        });
        //console.log('Verification:');
        //console.log(levels);
    }
    //
    //function calcIntegralIndex(root, maxLevel) {
    //    var result = 0.0;
    //    for (var level = 1; level < maxLevel; ++level) {
    //        var product = 0.0;
    //        var items = root.items;
    //        items.forEach(function (item) {
    //            product += Math.pow(item.Q, level - 1) * Math.pow(item.q, level);
    //            //console.log('Q:' + item.Q, 'q:' + item.q,
    //                'Math.pow(item.Q, level - 1) * Math.pow(item.q, level):' + Math.pow(item.Q, level - 1) * Math.pow(item.q, level),
    //                'product:' + product, 'level:' + level);
    //        });
    //        result += product;
    //    }
    //    //console.log(result);
    //    return result;
    //}


    function getMetricForNode(nodeNumber, metrics) {
        console.log(nodeNumber);
        // console.log(metrics);
        if (metrics == undefined ||
            metrics == null ||
            metrics['availableNodes'] == null ||
            metrics['availableNodes'] == undefined) {
            return null;
        }
        if (!~metrics.availableNodes.indexOf(nodeNumber))
            return null;

        for (var i in metrics.items) {
            if (metrics.items[i].nodeNumber == nodeNumber) {
                return metrics.items[i];
            }
        }
    }

    function getMetricIndexForNode(nodeNumber, metrics) {
        var result = getMetricForNode(nodeNumber, metrics);
        console.log(result ? +result.intergralIndex : 1);
        return result ? +result.intergralIndex : 1;
    }

    function setNodeLevelsAndGetMaxLevel(root) {
        var maxLevel = 0;
        function traceTree(root, level) {
            if (root.items.length) {
                root.items.forEach(function (item) {
                    traceTree(item, level + 1);
                });
            }
            if (maxLevel < level) {
                maxLevel = level;
            }
            root.level = level;
        }

        traceTree(root, 0);
        return maxLevel;
    }

    function average(items) {
        var sum = 0.0;
        items.forEach(function (item) {
            sum += item;
        });
        return sum / items.length;
    }

})(window, document);