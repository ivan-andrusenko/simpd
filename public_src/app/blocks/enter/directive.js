(function(window, document, undefined) {

'use strict';

angular.module('simpd').directive('simpdEnter', function() {
  return {
    restrict: 'EA',
    replace: true,
    templateUrl: 'app/blocks/enter/template.html',
    controller: ['$scope', '$rootScope', '$location', function($scope, $rootScope, $location) {
      $('.simpd-background').addClass('highlight');
      $scope.$on('$destroy', function() {
        $('.simpd-background').removeClass('highlight');
      });
      $rootScope.go = function (path) {
        $location.path(path);
      };

    }]
  };
});

})(window, document);