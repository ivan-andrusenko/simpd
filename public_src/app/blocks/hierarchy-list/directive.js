(function(window, document, undefined) {

'use strict';

angular.module('simpd').directive('simpdHierarchyList', function() {
  return {
    restrict: 'EA',
    replace: true,
    scope: {
      addItem: '&',
      source: '='
    },
    templateUrl: 'app/blocks/hierarchy-list/template.html',
    controller: ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope) {
      $scope.delete = function(item) {
        if (confirm('Удалить набор требований '+item.name+'?')) {
          $http.delete('/api/v0/requirements/'+item.id).then(function() {
            var i = $scope.source.indexOf(item);
            if (i >= 0) $scope.source.splice(i, 1);
          });
        }
      };

      $scope.go = $rootScope.go;
    }]
  };
});

})(window, document);