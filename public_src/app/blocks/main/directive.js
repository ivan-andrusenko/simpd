(function (window, document, undefined) {

  'use strict';

  angular.module('simpd').directive('simpdMain', function () {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: 'app/blocks/main/template.html',
      controller: ['$scope', '$translate', '$rootScope', '$location', 'webappSession', '$state',
        function ($scope, $translate, $rootScope, $location, webappSession, $state) {
          $scope.setLocale = function (langKey) {
            $translate.use(langKey);
          };
          $scope.getLocale = function () {
            return $translate.use();
          };
          $scope.logout = function () {
            webappSession.logout();
            $state.go('enter');
          };
          $rootScope.go = function (path) {
            $location.path(path);
          };

          setInterval(function () {
            var token = localStorage.getItem("token");
            if (!token) {
              if (!~["enter", "signin", "signup"].indexOf($state.$current.self.name)) {
                $state.go('enter');
              }
            }
          }, 100);
        }
      ]
    };
  })

})(window, document);