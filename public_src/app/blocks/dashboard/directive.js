(function(window, document, undefined) {

'use strict';

angular.module('simpd').directive('simpdDashboard', function() {
  return {
    restrict: 'EA',
    replace: true,
    templateUrl: 'app/blocks/dashboard/template.html'
  };
});

})(window, document);