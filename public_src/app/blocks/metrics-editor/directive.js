(function (window, document, undefined) {


  angular.module('simpd').directive('simpdMetricsEditor', function () {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: 'app/blocks/metrics-editor/template.html',
      scope: {
        value: '='
      },
      controller: ['$scope', '$state', '$http', '$element', '$attrs', '$location', '$rootScope', '$anchorScroll',
        function ($scope, $state, $http, $element, $attrs, $location, $rootScope, $anchorScroll) {
          $scope.treeOptions = {};
          // $scope.availableNodes = [];
          console.log($scope.value.requirements);
          $scope.value.availableNodes = getLeafItemAttributes($scope.value.requirements.items, "number");
          // $scope.value.formulas = {};
          // $scope.value.formulaViews = {};
          // $scope.value.valuesViews = {};
          // $scope.value.values = {};
          // $scope.value.intergralIndexes = {};
          // $scope.value.nodeNumbers = {};

          $scope.formulaEditMode = false;

          $scope.startFormulaEdit = function () {
            $scope.formulaEditMode = !$scope.formulaEditMode;
          };

          $scope.endFormulaEdit = function (nodeNumber) {
            $scope.formulaEditMode = !$scope.formulaEditMode;
            // $scope.value.formulaViews[nodeNumber] = "$$" + math.parse($scope.value.formulas[nodeNumber]).toTex({parenthesis: 'keep'}) + "$$";
            $scope.recalcIntegralIndex(nodeNumber);
          };

          $scope.valuesEditMode = false;

          $scope.startValuesEdit = function () {
            $scope.valuesEditMode = !$scope.valuesEditMode;
          };

          $scope.endValuesEdit = function (nodeNumber) {
            $scope.valuesEditMode = !$scope.valuesEditMode;
            // $scope.value.formulaViews[nodeNumber] = "$$" + math.parse($scope.value.formulas[nodeNumber]).toTex({parenthesis: 'keep'}) + "$$";
            $scope.recalcIntegralIndex(nodeNumber);
          };

          if ($scope.value.items == null) {
            $scope.value.items = [];
          }


          $scope.$watch('value', recalc, true);
          // $scope.$watch('value', recalc, true);
          // $scope.$watch('value', recalc, true);
          $scope.cancel = function () {
            $state.go('hierarchies');
          };

          $scope.scrollToBottom = function () {
            $location.hash('bottom');
            $anchorScroll();
          };
          $scope.save = function () {
            console.log('saving');
            var result;
            if ($scope.value.id) result = $http.put('/api/v0/metrics/' + $scope.value.id, $scope.value);
            else result = $http.post('/api/v0/metrics/', $scope.value);
            result.then(function () {
              $state.go('metrics');
            }, function (err) {
              console.log(err);
            });
          };
          $scope.addTopLevel = function () {
            $scope.scrollToBottom();
            $scope.value.items.push({
              formula: '',
              formulaView: '',
              valuesView: '',
              values: '',
              intergralIndex: '',
              nodeNumber: ''
            });
          };
          $scope.newSubItem = function (scope) {
            var modelValue = scope.$nodeScope.$modelValue;
            modelValue.items.push({
              name: '',
              items: []
            });
          };
          $scope.setNodeNumber = function (number, newValue) {
            number = newValue;
            console.log("number is now " + number);
          };
          $scope.recalcIntegralIndex = function (nodeNumber) {
            console.log(nodeNumber);
            console.log($scope.value.items);
            // var values = $scope.value.values[nodeNumber];
            // var formula = $scope.value.formulas[nodeNumber];
            var values = $scope.value.items[nodeNumber - 1].values;
            var formula = $scope.value.items[nodeNumber - 1].formula;
            var code = (values ? values.replace("\n", "; ") + "; " : "") + formula;
            console.log("code is:");
            console.log(code);
            var node = null;
            // $scope.value.intergralIndexes[nodeNumber] = eval(code);
            try {
              if (formula != null)
                node = math.parse(code);
              var formulaView = math.parse(formula);
              var valuesView = math.parse((values ? values.replace("\n", "; ") + "; " : ""));
              // $scope.value.intergralIndexes[nodeNumber] = node ? math.format(node.compile().eval()).replace('[', '').replace(']', '') : null;
              // $scope.value.formulaViews[nodeNumber] = formulaView ? formulaView.toTex({parenthesis: 'keep'}) : '';
              // $scope.value.valuesViews[nodeNumber] = valuesView ? valuesView.toTex({parenthesis: 'keep'}) : '';
              $scope.value.items[nodeNumber - 1].formulaView = formulaView ? formulaView.toTex({parenthesis: 'keep'}) : '';
              $scope.value.items[nodeNumber - 1].valuesView = valuesView ? valuesView.toTex({parenthesis: 'keep'}) : '';
              $scope.value.items[nodeNumber - 1].intergralIndex = node ? math.format(node.compile().eval()).replace('[', '').replace(']', '') : null;
            }
            catch (err) {
              $scope.value.items[nodeNumber - 1].intergralIndex = err.toString();
            }
          };
          $scope.removeSubItem = function (scope, nodeNumber) {
            return scope.$nodeScope.$parentNodesScope.removeNode(scope.$nodeScope);
          };
          // $scope.removeSubItem = (scope, nodeNumber) => (
          //      scope.$nodeScope.$parentNodesScope.removeNode(scope.$nodeScope);
          // );

          $scope.changeSelectedNodeNumber = function (item, model, previousNumber) {
            console.log("item");
            console.log(item);
            console.log("model");
            console.log(model);
            console.log("previousNumber");
            console.log(previousNumber);
            // $scope.value.availableNodes.push($scope.value.items[nodeNumber].nodeNumber);
          };

          $scope.showResults = function () {
            // $state.go('research/17/results', {id: $scope.value.requirements.id});
            $rootScope.go("research/" + $scope.value.requirements.id + "/results");
          };
          function recalc(value) {
            processLevel(value.items);
            function processLevel(items) {
              // if (!level) level = 0;
              var index = 1;
              items.forEach(function (item) {
                // if (!parentNumber)
                item.number = index;
                // else item.number = parentNumber+'.'+index;
                index++;
                // processLevel(item.items, item.number, level+1);
              });
            }
          }


          function getLeafItemAttributes(items, attr) {
            var res = [];
            // console.log("items");
            // console.log(items);
            items.forEach(function (item) {
              if (item.items.length == 0) {
                res.push(item[attr]);
                // console.log("item[attr]");
                // console.log(item[attr]);
                // console.log("res");
                // console.log(res);
              }
              else
                res.push.apply(res, getLeafItemAttributes(item.items, attr));
            });
            return res;
          }

          function getLowLevelItemAttributes(items, attr) {
            var res = [];
            var levels = [];
            items.forEach(function (item) {
              levels.push(item.level);
            });
            var lowLevel = Math.max.apply(Math, levels);
            console.log(lowLevel);
            items.forEach(function (item) {
              if (item.level == lowLevel)
                res.push(item[attr]);
            });
            return res;
          }
        }]
    };
  }).directive("mathjaxBind", function () {
    return {
      restrict: "A",
      controller: ["$scope", "$element", "$attrs", function ($scope, $element, $attrs) {
        $scope.$watch($attrs.mathjaxBind, function (value) {
          var $script = angular.element("<script type='math/tex'>")
            .html(value == undefined ? "" : value);
          $element.html("");
          $element.append($script);
          console.log("inside mathjaxBind");
          MathJax.Hub.Queue(["Reprocess", MathJax.Hub, $element[0]]);
        });
      }]
    };
  });

})(window, document);