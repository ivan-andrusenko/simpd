(function(window, document, undefined) {

'use strict';

angular.module('simpd').directive('simpdAuthors', function() {
  return {
    restrict: 'EA',
    replace: true,
    templateUrl: 'app/blocks/authors/template.html',
    controller: ['$scope', '$rootScope', function ($scope, $rootScope) {
      $scope.go = $rootScope.go;
    }
    ]
  };
});

})(window, document);