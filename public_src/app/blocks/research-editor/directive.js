(function (window, document, undefined) {

  'use strict';

  angular.module('simpd').directive('simpdResearchEditor', function () {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: 'app/blocks/research-editor/template.html',
      scope: {
        value: '=value'
      },
      controller: ['$scope', '$state', '$modal', '$http', '$q', 'webappSession', function ($scope, $state, $modal, $http, $q, webappSession) {
        $scope.addEvaluation = function () {
          saveNewResearch()
            .then(function (researchId) {
              webappSession.getUser()
                .then(function (user) {
                  console.log(user);
                  if (user.role == 'user') {
                    console.log("$state.go('researches_evaluation_editor', {researchId: researchId, evaluationId: 'new'});");
                    $state.go('researches_evaluation_editor', {researchId: researchId, evaluationId: 'new'});
                  } else {
                    var options = {
                      content: "Only experts can evaluate",
                      style: "snackbar",
                      timeout: 6000
                    };
                    $.snackbar(options);
                  }
                });
            });
        };
        $scope.showResults = function () {
          $state.go('research_results', {id: $scope.value.id});
        };
        function saveNewResearch() {
          if ($scope.value.id) {
            var deferred = $q.defer();
            deferred.resolve($scope.value.id);
            return deferred.promise;
          }
          else {
            return $http.post('/api/v0/researches', $scope.value).then(function (result) {
              return result.data.id;
            });
          }
        }
      }]
    };
  });

})(window, document);