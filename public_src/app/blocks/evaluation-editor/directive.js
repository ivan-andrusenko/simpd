(function(window, document, undefined) {

'use strict';

angular.module('simpd').directive('simpdEvaluationEditor', function() {
  return {
    restrict: 'EA',
    replace: true,
    templateUrl: 'app/blocks/evaluation-editor/template.html',
    scope: {
      value: '='
    },
    controller: ['$scope', '$state', '$http', function($scope, $state, $http) {
      $scope.view = 'tree';
      var flatItems = [];
      processLevel($scope.value.requirements.items);
      $scope.items = flatItems;
      function processLevel(items, level) {
        if (!level) level = 0;
        items.forEach(function(item) {
          flatItems.push({
            level: level,
            number: item.number,
            name: item.name,
            rate: item.rate,
            value: item.value
          });
          if (item.items) processLevel(item.items, level+1);
        });
      }
    }]
  };
});

})(window, document);