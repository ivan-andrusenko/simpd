(function(window, document, undefined) {

'use strict';

angular.module('simpd').directive('simpdHierarchyEditor', function() {
  return {
    restrict: 'EA',
    replace: true,
    templateUrl: 'app/blocks/hierarchy-editor/template.html',
    scope: {
      value: '='
    },
    controller: ['$scope', '$state', '$http', '$location', '$anchorScroll', function($scope, $state, $http, $location, $anchorScroll) {
      $scope.treeOptions = {
      };
      $scope.$watch('value', recalc, true);
      $scope.cancel = function() {
        $state.go('hierarchies');
      };
      $scope.save = function() {
        console.log('saving');
        var result;
        if ($scope.value.id) result = $http.put('/api/v0/requirements/'+$scope.value.id, $scope.value);
        else result = $http.post('/api/v0/requirements/', $scope.value);
        result.then(function() {
          $state.go('hierarchies');
        }, function(err) {
          console.log(err);
        });
      };
      $scope.addTopLevel = function() {
        $scope.scrollToBottom();
        $scope.value.items.push({
          name: '',
          items: []
        });
      };
      $scope.newSubItem = function (scope) {
        var modelValue = scope.$nodeScope.$modelValue;
        modelValue.items.push({
          name: '',
          items: []
        });
      };
      $scope.scrollToBottom = function() {
        $location.hash('bottom');
        $anchorScroll();
      };
      $scope.removeSubItem = function (scope) {
         return scope.$nodeScope.$parentNodesScope.removeNode(scope.$nodeScope);
      };
      function recalc(value) {
        processLevel(value.items);
        function processLevel(items, parentNumber, level) {
          if (!level) level = 0;
          var index = 1;
          items.forEach(function(item) {
            if (!parentNumber) item.number = index;
            else item.number = parentNumber+'.'+index;
            index++;
            processLevel(item.items, item.number, level+1);
          });
        }
      }
    }]
  };
});

})(window, document);