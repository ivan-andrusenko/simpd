(function (window, document, undefined) {

  'use strict';

  angular.module('simpd').directive('simpdSignup', function () {
    return {
      restrict: 'EA',
      replace: true,
      scope: true,
      templateUrl: 'app/blocks/signup/template.html',
      controller: ['$scope', '$http', '$state', 'webappSession', function ($scope, $http, $state, webappSession) {
        $scope.credentials = {
          role: 'user'
        };
        $scope.signup = function () {
          webappSession.signup($scope.credentials).then(function (res) {
            console.log(res);
            if (res.error) {
              var options = {
                content: res.error,
                style: "snackbar", // add a custom class to your snackbar
                timeout: 6000
              };
              $.snackbar(options);
            } else {
              $state.go('signin');
            }
          });
        };

        $http.get('/api/v0/fields').then(function (items) {
          $scope.fields = items.data;
        });
      }]
    };
  });

})(window, document);