(function (window, document, undefined) {

    'use strict';

    angular.module('simpd').directive('simpdRequirementsTree', function () {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: 'app/blocks/requirements-tree/template.html',
            scope: {
                value: '='
            },
            link: function ($scope, $element) {
                $scope.$watch('value', render);
                function render(value) {
                    value = convert(value[0]);
                    $element.empty();
                    var margin = {top: 20, right: 120, bottom: 20, left: 120},
                        width = 960 - margin.right - margin.left,
                        height = 1000 - margin.top - margin.bottom;

                    var tree = d3.layout.tree()
                        .separation(function () {
                            return 2;
                        })
                        .nodeSize([15, 70]);

                    var diagonal = d3.svg.diagonal()
                        .projection(function (d) {
                            return [d.y, d.x];
                        });

                    var svg = d3.select($element[0]).append("svg")
                        .attr("width", '100%')
                        .attr("height", height);
                    var h = $element.height();
                    var svgP = svg.append("g")
                        .attr("transform", "translate(40, " + (h / 2 | 1) + ")");
                    var svgGroup = svgP.append("g");
                    var tooltipGroup = svgP.append("g");

                    var nodes = tree.nodes(value),
                        links = tree.links(nodes);

                    var link = svgGroup.selectAll("path.link")
                        .data(links)
                        .enter().append("path")
                        .attr("class", "link")
                        .attr("d", diagonal);

                    var node = svgGroup.selectAll("g.node")
                        .data(nodes)
                        .enter().append("g")
                        .attr("class", "node")
                        .attr("transform", function (d) {
                            return "translate(" + d.y + "," + d.x + ")";
                        })
                        .on('mouseover', function (d) {
                            var g = tooltipGroup;
                            g = g.append('g')
                                .attr('class', 'node__tooltip');
                            var info = g.append('rect')
                                .attr('class', 'node__tooltip')
                                .attr('width', 10 * d.name.length)
                                .attr('height', 30)
                                .attr('fill', 'white')
                                .attr('stroke', 'black')
                                .attr('stroke-width', 1)
                                .attr('x', d.y)
                                .attr('y', d.x);
                            console.log(d);
                            info = g.append('text')
                                .attr('class', 'node__tooltip')
                                .attr('x', d.y + 20)
                                .attr('y', d.x + 20)
                                .text(d.name);
                        })
                        .on('mouseout', function (d) {
                            tooltipGroup.select('.node__tooltip').remove();
                        });

                    node.append("circle")
                        .attr("r", 4.5);

                    node.append("text")
                        .attr("dx", function (d) {
                            return d.children ? -8 : 8;
                        })
                        .attr("dy", function (d) {
                            if (d.Q == null) return 3;
                            else return -3;
                        })
                        .attr("text-anchor", function (d) {
                            return d.children ? "end" : "start";
                        })
                        .text(function (d) {
                            return d.number ? 'T ' + d.number : d.name
                        });
                    node.append("text")
                        .attr("dx", function (d) {
                            return d.children ? -8 : 8;
                        })
                        .attr("dy", 10)
                        .attr("text-anchor", function (d) {
                            return d.children ? "end" : "start";
                        })
                        .text(function (d) {
                            return (d.Q.toPrecision(3) ? ' Q:' + d.Q.toPrecision(3) : '');
                        });

                    d3.select(self.frameElement).style("height", height + "px");
                    svg.call(d3.behavior.zoom().on("zoom", function redraw() {
                        svgP.attr("transform",
                            "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
                    }));
                }

                function convert(value) {
                    var result = {};
                    result.name = value.name;
                    result.Q = value.Q;
                    processChildren(result, value.items);
                    return result;
                    function processChildren(parent, items) {
                        if (!items) return;
                        if (!parent.children) parent.children = [];
                        items.forEach(function (item) {
                            var r = {};
                            r.number = item.number;
                            r.name = item.name;
                            r.Q = item.Q;
                            processChildren(r, item.items);
                            parent.children.push(r);
                        });
                    }


                }
            }
        }
    });

})(window, document);