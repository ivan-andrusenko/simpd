(function(window, document, undefined) {

var msDay = 1000*60*60*24;

function pad(v, len) {
  v = v.toString();
  while (v.length < len) {
    v = '0'+v;
  }
  return v;
}

angular.module('webapp').filter('webappDateToday', function() {
  return function(value) {
    if (!value) return '';
    var now = new Date();
    var diff = now-value;
    var hours = pad(value.getHours(), 2);
    var minutes = pad(value.getMinutes(), 2);
    var seconds = pad(value.getSeconds(), 2);
    var result = hours+':'+minutes+':'+seconds;
    if (Math.abs(diff) > msDay) {
      var day = pad(value.getDate(), 2);
      var month = pad(value.getMonth()+1, 2);
      var year = value.getYear();
      result = day+'.'+month+'.'+year+' '+result;
    }
    return result;
  };
});

})(window, document);