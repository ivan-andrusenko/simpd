(function(window, document, undefined) {

'use strict';

angular.module('webapp').service('webappExcelGenerator', function() {
  this.generate = function(info, value) {
    var result = [
      '<?xml version="1.0"?>',
      '<?mso-application progid="Excel.Sheet"?>',
      '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">'
    ];
    // props
    result.push('<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">');
    result.push('<Author>'+(info && info.author ? info.author : '')+'</Author>');
    result.push('<LastAuthor>'+(info && info.author ? info.author : '')+'</LastAuthor>');
    result.push('<Created>'+(new Date().toISOString())+'</Created>');
    result.push('</DocumentProperties>');
    // styles
    result.push('<ss:Styles><ss:Style ss:ID="1"><ss:Font ss:Bold="1"/></ss:Style></ss:Styles>');
    // sheet
    result.push('<ss:Worksheet ss:Name="Sheet1">');
    result.push('<ss:Table>');
    // columns
    for (var k=0; k<value[0].length;k++) {
      result.push('<ss:Column/>');
            //<ss:Column ss:Width="80"/>
            //<ss:Column ss:Width="80"/>
    }
    value.forEach(function(row) {
      if (value[0] == row) result.push('<ss:Row ss:StyleID="1">');
      else result.push('<ss:Row>');
      row.forEach(function(field) {
        result.push('<ss:Cell>');
        var type = 'String';
        if (field && !isNaN(field)) type = 'Number';
        result.push('<ss:Data ss:Type="'+type+'">'+field+'</ss:Data>');
        result.push('</ss:Cell>');
      });
      result.push('</ss:Row>');
    });
    //
    result.push('</ss:Table>');
    result.push('</ss:Worksheet>');
    //
    result.push('</Workbook>');
    return result.join('\x0d\x0a');
  };
});

})(window, document);