(function(window, document, undefined) {

'use strict';

angular.module('webapp').provider('webappSession', ['$injector', function ($injector) {
  var url = '/api/auth/signin';
  var map = null;
  var token = localStorage.getItem("token");
  this.url = function(value) {
    if (!value) return url;
    url = value;
    return this;
  };
  this.map = function(value) {
    if (!value) return map;
    map = value;
    return this;
  };
  this.token = function(value) {
    if (!value) return token;
    token = value;
    return this;
  };
  $injector.get('$httpProvider').defaults.headers.common.Authorization = token;
  this.$get = ['$http', function($http) {
    return new Session($http, url, map, token);
  }];
}]);

function Session($http, url, map, token) {
  this.$http = $http;
  this.url = url;
  this.signUpUrl = '/api/auth/signup';
  this.memberInfoUrl = '/api/auth/memberinfo';
  this.map = map;
  this.token = token;
  this.userName = null;
  this.user = null;
  this.setToken(token);
  if (token) {
    this.setToken(token);
  }
}

Session.prototype.signin = function(login, password) {
  var params = {name: login, password: password};
  if (this.map) params = this.map(params);
  var thisToken = this.token;
  var self = this;
  this.userName = login;
  return this.$http.post(this.url, params).then(function(res) {
    var error = null;
    var token = null;
    if (res.data.success == true) {
      token = res.data.token;
    } else {
      error = res.data.msg;
    }
    thisToken = token;
    self.setToken(thisToken);
    return {error: error, token: thisToken}
  });
};
Session.prototype.signup = function(credentials) {
  var params = credentials;
  if (this.map) params = this.map(params);
  var thisToken = this.token;
  var self = this;
  return this.$http.post(this.signUpUrl, params).then(function(res) {
    var error = null;
    var success = res.data.success;
    if (!success) {
      error = res.data.msg;
    }
    return {error: error, success: success}
  });
};

Session.prototype.logout = function() {
  this.setToken(null);
  this.user = null;
  this.userName = null;
  localStorage.removeItem('token');
};

Session.prototype.setToken = function(token) {
  this.token = token;
  this.$http.defaults.headers.common.Authorization = this.token;
  localStorage.setItem("token", this.token);
};

Session.prototype.getUser = function() {
  console.log("before this.getInfo(this.token).user");
  if (this.user) {
    return this.user;
  }
  this.user = this.getInfo(this.token);
  return this.user;
};

Session.prototype.getInfo = function (token) {
  console.log(token);
  console.log(this.$http);
  console.log("before return this.$http.get(this.memberInfoUrl)");
  return this.$http.get(this.memberInfoUrl)
    .then(function (res) {
      console.log("before console.log(res);");
      console.log(res);
      return res.data;
    }, function (err) {
      console.log(err);
    })
};

})(window, document);