'use strict';

var http = require('http');
var morgan = require('morgan');
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var dbConfig = require('./config/database'); // get db config file
var User = require('./app/models/user'); // get the mongoose model
var log4js = require('log4js');
var jwt = require('jwt-simple');
var mongoose = require('mongoose');

var logger = log4js.getLogger('simpd');

var config = require('./config/config.json');

var Database = require('./lib/database').Database;

var db = new Database(__dirname + '/lib/data.json');


startWebApi();

function startWebApi() {
  var app = express();

  app.use(bodyParser.json());
  app.use(morgan('dev'));
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(cookieParser());

  app.use(passport.initialize());
  app.use(express.static(__dirname + '/public'));

  mongoose.connect(dbConfig.database);
  require('./config/passport')(passport);

  var apiRoutes = express.Router();

  function auth(fn) {
    return function(req, res, next) {
      if (req.path == '/v0/fields') {
        next();
      } else {
        fn(req, res, next);
      }
    }
  }

  apiRoutes.route('/v0/:model/:id?')
    .all(bodyParser.json({limit: 10 * 1024 * 1024}))
    .all(auth(passport.authenticate('jwt', {session: false})))
    .all(function (req, res) {
      var result;
      if (req.method == 'POST') {
        result = db.create(req.params.model, req.body);
        res.status(201).json({id: result}).end();
      }
      else if (req.method == 'GET') {
        result = db.read(req.params.model, req.params.id);
        res.json(result).end();
      }
      else if (req.method == 'PUT') {
        result = db.write(req.params.model, req.body);
        res.json({id: result}).end();
      }
      else if (req.method == 'DELETE') {
        result = db.delete(req.params.model, req.params.id);
        res.end();
      }
      else {
        res.status(400).json({error: 'unknown method ' + req.method}).end();
      }
    });

  apiRoutes.post('/auth/signup', function (req, res) {
    if (!req.body.name || !req.body.password || !req.body.info || !req.body.role) {
      res.json({success: false, msg: 'Please pass name, password, role and all the additional info.'});
    } else {
      var newUser = new User({
        name: req.body.name,
        password: req.body.password,
        info: req.body.info,
        role: req.body.role
      });
      newUser.save(function (err) {
        if (err) {
          return res.json({success: false, msg: 'Username already exists.'});
        }
        res.json({success: true, msg: 'Successful created new user.'});
      });
    }
  });

  apiRoutes.post('/auth/signin', function (req, res) {
    User.findOne({
      name: req.body.name
    }, function (err, user) {
      if (err) throw err;

      if (!user) {
        res.send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.encode(user, dbConfig.secret);
            // return the information including token as JSON
            res.json({success: true, token: 'JWT ' + token});
          } else {
            res.send({success: false, msg: 'Authentication failed. Wrong password.'});
          }
        });
      }
    });
  });

  apiRoutes.get('/auth/memberinfo', passport.authenticate('jwt', {session: false}), function (req, res) {
    var token = getToken(req.headers);
    if (token) {
      var decoded = jwt.decode(token, dbConfig.secret);
      User.findOne({
        name: decoded.name
      }, function (err, user) {
        console.log(err, user);
        if (err) throw err;

        if (!user) {
          return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
          console.log("Success!");
          res.json(user);
        }
      });
    } else {
      return res.status(403).send({success: false, msg: 'No token provided.'});
    }
  });

  var getToken = function (headers) {
    // console.log(headers);
    if (headers && headers.authorization) {
      var parted = headers.authorization.split(' ');
      // console.log(parted);
      if (parted.length === 2) {
        // console.log(parted[1]);
        return parted[1];
      } else {
        return null;
      }
    } else {
      return null;
    }
  };


  app.use('/api', apiRoutes);

  var server = http.createServer(app);
  server.listen(config.server.port, function () {
    logger.info('listening ' + config.server.port);
  });
}
