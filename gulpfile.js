'use strict';

var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var del = require('del');
var rimraf = require('rimraf');
var frontend = require('./build/frontend');

var source = './public_src';
var destination = './public';

var fileNameIndexHtml = 'index.html';

var frontendOptions = {
    base: __dirname,
    source: source,
    destination: destination,
    indexHtml: fileNameIndexHtml
};

gulp.task('clean', function (done) {
    rimraf(destination, done);
    //del([destination], function() {
    //console.log('DONE');
    //});
});

gulp.task('build-frontend', ['clean'], function (done) {
    frontend(frontendOptions)(done);
});

gulp.task('production', ['clean'], function (done) {
    frontendOptions.production = true;
    frontend(frontendOptions)(done);
});

gulp.task('default', ['build-frontend']);

gulp.task('watch', ['build-frontend'], function (done) {
    var that = this;

    function iterate() {
        frontend.watch(frontendOptions).then(function () {
            console.log('changed');
            var task = gulp.start('build-frontend');
            task.on('task_stop', function () {
                task.removeAllListeners();
                setImmediate(iterate);
            });
        });
    };
    iterate();
});
