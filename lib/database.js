'use strict';

var fs = require('fs');

// Database

function Database(name) {
    this.name = name;
}

Database.prototype.create = function (name, value) {
    var data = this._load();
    if (!data.tables) data.tables = {};
    var table = data.tables[name];
    if (!table) table = data.tables[name] = {};
    if (!table.items) table.items = [];
    if (!table.id) table.id = 1;
    var id = table.id++;
    value.id = id;
    table.items.push(value);
    this._save(data);
    return id;
};

Database.prototype.read = function (name, id) {
    var data = this._load();
    if (!data.tables) return null;
    var table = data.tables[name];
    if (!table) return null;
    var result = table.items;
    if (!result) return null;
    if (id) result = result.filter(function (row) {
        return row.id == id;
    });
    if (id) return result[0];
    else return result;
};

Database.prototype.write = function (name, value) {
    var data = this._load();
    if (!data.tables) data.tables = {};
    var table = data.tables[name];
    if (!table) table = data.tables[name] = {};
    if (!table.items) table.items = [];
    for (var k = 0; k < table.items.length; k++) {
        var row = table.items[k];
        if (row.id == value.id) {
            table.items.splice(k, 1, value);
            this._save(data);
            return value.id;
        }
    }
};

Database.prototype.delete = function (name, id) {
    var data = this._load();
    if (!data.tables) data.tables = {};
    var table = data.tables[name];
    if (!table) table = data.tables[name] = {};
    if (!table.items) table.items = [];
    for (var k = 0; k < table.items.length; k++) {
        var row = table.items[k];
        if (row.id == id) {
            table.items.splice(k, 1);
            this._save(data);
            return id;
        }
    }
};

Database.prototype._load = function () {
    return JSON.parse(fs.readFileSync(this.name));
};

Database.prototype._save = function (value) {
    fs.writeFileSync(this.name, JSON.stringify(value, null, 2));
};

//

module.exports.Database = Database;